
package org.openshift.quickstarts.undertow.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.Thread;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RemoteTimeOutServlet extends HttpServlet {

    private static Logger LOGGER=Logger.getLogger(RemoteTimeOutServlet.class.getName());

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
      try {
        PrintWriter writer = resp.getWriter();
        int sec=0;
        try {
            sec = Integer.parseInt(req.getParameter("sec"));
        }
        catch (Exception e) {
            sec=0;
        }
        String url="https://testjava-git-jlarroyoc-dev.apps.sandbox-m2.ll9k.p1.openshiftapps.com/timeout?sec="+sec;

        LOGGER.info("["+Thread.currentThread().getId()+"] Calling remote service: "+url);
        String out=this.getURL(url);
        writer.write(out);
        LOGGER.info("["+Thread.currentThread().getId()+"] result:"+out);
        writer.close();
        }
        catch (Exception e) {
           
            // catching the exception
            System.out.println(e);
        }
    }

    public String getURL(String url) {
 
        Client client = ClientBuilder.newClient();
 
        WebTarget resource = client.target(url);
        return resource.request().get().readEntity(String.class);
    }
}
