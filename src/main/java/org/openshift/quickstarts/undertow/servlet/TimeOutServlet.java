
package org.openshift.quickstarts.undertow.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.Thread;
import java.util.logging.Logger;


public class TimeOutServlet extends HttpServlet {

    private static Logger LOGGER=Logger.getLogger(TimeOutServlet.class.getName());

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
      try {
        PrintWriter writer = resp.getWriter();
        int sec=0;
        try {
            sec = Integer.parseInt(req.getParameter("sec"));
        }
        catch (Exception e) {
            sec=0;
        }
        LOGGER.info("["+Thread.currentThread().getId()+"] timeout start:"+sec);
        Thread.sleep(sec*1000);
        writer.write("{\"seconds\":"+sec+"}");
        LOGGER.info("["+Thread.currentThread().getId()+"] timeout finish");
        writer.close();
        }
        catch (Exception e) {
           
            // catching the exception
            System.out.println(e);
        }
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("["+Thread.currentThread().getId()+"] POST method...");
        doGet(req, resp);
    }
}
